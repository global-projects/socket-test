<?php

namespace App\Controller;

use App\Entity\Deneme;
use App\Form\DenemeType;
use App\Repository\DenemeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/deneme")
 */
class DenemeController extends AbstractController
{
    /**
     * @Route("/", name="deneme_index", methods={"GET"})
     */
    public function index(DenemeRepository $denemeRepository): Response
    {
        return $this->render('deneme/index.html.twig', [
            'denemes' => $denemeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="deneme_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $deneme = new Deneme();
        $form = $this->createForm(DenemeType::class, $deneme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($deneme);
            $entityManager->flush();

            return $this->redirectToRoute('deneme_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('deneme/new.html.twig', [
            'deneme' => $deneme,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="deneme_show", methods={"GET"})
     */
    public function show(Deneme $deneme): Response
    {
        return $this->render('deneme/show.html.twig', [
            'deneme' => $deneme,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="deneme_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Deneme $deneme): Response
    {
        $form = $this->createForm(DenemeType::class, $deneme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('deneme_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('deneme/edit.html.twig', [
            'deneme' => $deneme,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="deneme_delete", methods={"POST"})
     */
    public function delete(Request $request, Deneme $deneme): Response
    {
        if ($this->isCsrfTokenValid('delete'.$deneme->getid(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($deneme);
            $entityManager->flush();
        }

        return $this->redirectToRoute('deneme_index', [], Response::HTTP_SEE_OTHER);
    }
}
